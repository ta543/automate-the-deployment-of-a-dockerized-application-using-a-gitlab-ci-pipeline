# CryptoDataApp Project 🌟

## Introduction 📘

CryptoDataApp is a comprehensive solution designed to fetch, process, and display cryptocurrency exchange rates, focusing on the Bitcoin to Czech Koruna (BTC to CZK) conversion rate. This project automates the deployment of a dockerized Python application using GitLab CI/CD pipelines, emphasizing Infrastructure as Code (IaC) practices with Terraform for cloud resource management.

## Project Structure 🏗

The project is structured as follows to ensure modularity and ease of maintenance:

- `data_downloader/`: Contains scripts for downloading exchange rate data.
- `scripts/`: Includes `download_data.sh` for fetching data and `process_data.py` for extracting and processing data.
- `docker/`: Houses the Dockerfile for building the application container.
- `iac/`: Contains Terraform configurations for provisioning cloud resources.
- `web/`: Contains the `index.html` file, which is the frontend display of the fetched data.
- `.gitlab-ci.yml`: Defines the CI/CD pipeline for automating code linting, building, testing, and deploying.

## Approach 🚀

### Data Handling 📊

The application utilizes scheduled tasks to regularly download the latest exchange rates from the Coinbase API. The `download_data.sh` script fetches the data, which is then processed by `process_data.py` to extract the relevant BTC to CZK rate. The processed data is displayed in a simple HTML page, `index.html`, served from an AWS S3 bucket configured for public access.

### Dockerization 🐳

The application is containerized using Docker to ensure consistency across different environments and simplify deployment. The Dockerfile in the `docker/` directory specifies a lightweight Python image and includes steps to install dependencies, copy source code, and define the entry point for the application.

### Infrastructure as Code (IaC) 🏗️

Terraform is used to define and provision the required cloud infrastructure in a repeatable and efficient manner. The configuration in the `iac/` directory sets up an AWS EC2 instance for running the application and an S3 bucket for storing and serving the processed data.

### Continuous Integration and Deployment 🔄

GitLab CI/CD pipelines are configured to automate the deployment process. The `.gitlab-ci.yml` file outlines several stages, including linting Python code to enforce coding standards, building the Docker image, and deploying the application to the provisioned cloud environment using Terraform.

## Working Result ✅

The deployed application successfully demonstrates the ability to automatically fetch, process, and display the BTC to CZK exchange rate on a web page hosted on AWS S3, accessible publicly.

## Challenges and Todos 🚧

### Challenges

- **Data API Limitations:** The free tier of the Coinbase API has rate limits that required careful handling to avoid service disruptions.
- **Cloud Resource Permissions:** Configuring the correct IAM roles and policies for the EC2 instance to access the S3 bucket took additional research and testing.
- **Docker Image Optimization:** The initial Docker image was larger than necessary, requiring optimization to reduce build and deployment times.

### Todos

- **Implement Caching:** To reduce API calls and improve responsiveness, implementing caching for the exchange rate data is planned.
- **Enhance Security:** Further hardening of cloud resources and the application, including more robust IAM policies and network security groups.
- **Expand Currency Support:** Extend the application to support more cryptocurrencies and fiat currencies.
