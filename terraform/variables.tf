variable "region" {
  description = "The AWS region to deploy resources into"
  type        = string
  default     = "eu-west-2"
}

variable "ami_id" {
  description = "The AMI ID to use for the EC2 instance"
  type        = string
  default     = "ami-0c02fb55956c7d316"
}

variable "key_name" {
  description = "The key pair name to use for the EC2 instance SSH access"
  type        = string
  default     = "~./acme-project/acme-key-pair.pem"
}


