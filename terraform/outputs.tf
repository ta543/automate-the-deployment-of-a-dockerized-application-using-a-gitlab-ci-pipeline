output "s3_bucket_name" {
  description = "The name of the S3 bucket used for storing application data."
  value       = aws_s3_bucket.data_bucket.bucket
}

output "ec2_instance_public_ip" {
  description = "The public IP address of the EC2 instance hosting the application."
  value       = aws_instance.app_instance.public_ip
}
