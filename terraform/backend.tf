terraform {
  backend "s3" {
    bucket         = "acme-corp-terraform-state-2024"
    key            = "terraform.tfstate"
    region         = "eu-west-2"
    dynamodb_table = "acme-corp-terraform-locks"
    encrypt        = true
  }
}

