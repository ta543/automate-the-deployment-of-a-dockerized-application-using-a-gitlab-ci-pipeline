resource "aws_instance" "app_instance" {
  ami           = var.ami_id
  instance_type = "t2.micro"
  key_name      = var.key_name

  tags = {
    Name = "AppInstance"
  }
}

resource "aws_s3_bucket" "data_bucket" {
  bucket = "my-app-data-bucket-${random_id.bucket_suffix.hex}"
  acl    = "public-read"

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  lifecycle {
    prevent_destroy = false
  }
}

resource "random_id" "bucket_suffix" {
  byte_length = 2
}

