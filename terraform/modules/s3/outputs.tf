output "instance_id" {
  description = "The ID of the EC2 instance."
  value       = aws_instance.app_instance.id
}

output "instance_public_ip" {
  description = "The public IP of the EC2 instance."
  value       = aws_instance.app_instance.public_ip
}

output "s3_bucket_arn" {
  description = "The ARN of the S3 bucket."
  value       = aws_s3_bucket.data_bucket.arn
}

output "s3_bucket_website_endpoint" {
  description = "The website endpoint URL of the S3 bucket."
  value       = aws_s3_bucket.data_bucket.website_endpoint
}

output "s3_bucket_name_suffix" {
  description = "The generated suffix for the S3 bucket name."
  value       = random_id.bucket_suffix.hex
}
