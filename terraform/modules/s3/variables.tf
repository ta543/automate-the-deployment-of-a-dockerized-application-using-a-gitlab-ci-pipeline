variable "ami_id" {
  description = "The AMI ID to use for the instance."
  type        = string
  default     = "ami-732832hdwuhdw"
}

variable "key_name" {
  description = "The key name to use for the instance for SSH access."
  type        = string
  default     = "my-default-key-pair"
}
