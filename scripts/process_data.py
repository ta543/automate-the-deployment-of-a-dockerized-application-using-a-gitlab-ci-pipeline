import json

def process_data(input_file="exchange_rates.json", output_file="index.html"):
    # Read the JSON data from the file
    with open(input_file, 'r') as file:
        data = json.load(file)
    
    # Extract BTC to CZK exchange rate
    btc_to_czk = data["data"]["rates"]["CZK"]

    # Generate an HTML file to display the data
    with open(output_file, 'w') as file:
        file.write("<html>\n<head><title>Exchange Rates</title></head>\n<body>\n")
        file.write("<h1>BTC to CZK Exchange Rate</h1>\n")
        file.write("<table border='1'>\n<tr><th>Currency</th><th>Rate</th></tr>\n")
        file.write(f"<tr><td>BTC to CZK</td><td>{btc_to_czk}</td></tr>\n")
        file.write("</table>\n</body>\n</html>")

    print(f"Data processed and HTML file generated at {output_file}")

if __name__ == "__main__":
    process_data()
