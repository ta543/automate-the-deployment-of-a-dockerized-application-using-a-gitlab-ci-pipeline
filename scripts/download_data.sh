#!/bin/bash

# Set the API URL for Coinbase exchange rates
API_URL="https://api.coinbase.com/v2/exchange-rates?currency=BTC"

# Define the output file
OUTPUT_FILE="exchange_rates.json"

# Use curl to fetch the data and store it in a file
curl -s "$API_URL" -o "$OUTPUT_FILE"

echo "Data downloaded and saved to $OUTPUT_FILE"
