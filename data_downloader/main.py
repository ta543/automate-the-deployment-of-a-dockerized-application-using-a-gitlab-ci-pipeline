import requests
import datetime

def fetch_exchange_rates():
    """
    Fetches the current exchange rates from the Coinbase API.
    """
    response = requests.get("https://api.coinbase.com/v2/exchange-rates?currency=BTC")
    if response.status_code == 200:
        return response.json()['data']['rates']
    else:
        print("Failed to fetch exchange rates")
        return None

def save_data_to_file(data, filename="exchange_rates.txt"):
    """
    Saves the extracted data to a local file. In a real scenario, this could be replaced with
    saving to cloud storage.
    """
    with open(filename, "a") as file:
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        file.write(f"{timestamp}: BTC to CZK = {data['CZK']}\n")

def generate_html_page(data, filename="index.html"):
    """
    Generates a simple HTML page displaying the data in a table format.
    """
    with open(filename, "w") as file:
        file.write("<html>\n<head><title>Exchange Rates</title></head>\n<body>\n")
        file.write("<h2>Bitcoin to Czech Koruna Exchange Rates</h2>\n")
        file.write("<table border='1'>\n<tr><th>Timestamp</th><th>Rate</th></tr>\n")
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        file.write(f"<tr><td>{timestamp}</td><td>{data['CZK']}</td></tr>\n")
        file.write("</table>\n</body>\n</html>")

def main():
    rates = fetch_exchange_rates()
    if rates:
        save_data_to_file(rates)
        generate_html_page(rates)
    else:
        print("No data to process")

if __name__ == "__main__":
    main()
